# LITK Wordpress Dev Test 1.0

*Basic development task for shortlisted candidates.*  

**Est. duration:** 4-6 hours  
**Required skills:** PHP, JavaScript, CSS3, HTML5, Git

## Task

Implement an object ­oriented [Wordpress 4.6+](https://wordpress.org/) plugin called ```litk-wp-test-<yourgithubname>```” that can be used as a Wordpress Widget and as a Wordpress Shortcode (naming convention: ```litk-wp-test-<yourgithubname>```).

It should display all subpages of the page it is placed on.

Choose an appropriate and easy to understand visual format.
Fetch and display all subpages titles, truncated after 20 characters and (if present) show a very small thumbnail­ of the pages 'featured image' next to the title.

Each level should be able to be sorted by title in ascending and descending order by the user. It should also be possible expand and collapsed each level.

All this must be possible **without** reconnecting to the
server after the page has been loaded.

## Details

Use LITKT Code guidelines when you write your code;    https://bitbucket.org/lexiconitkonsult/litk-code-guidelines

Keep in mind your code needs to be maintained by an international team of developers. Show us that you understand what this means in a wider context. If you have time, include a basic unit test for the PHP code. ([Need help?](https://bitbucket.org/lexiconitkonsult/litk-code-guidelines/src/ff8a3fa2b74981fee5e235539753d0ab1843e72e/help/PHPUnitTest.md))

**Make sure to:**

- Have absolutely no markup in your php-classes.
- Use Composer for class autoloader and any PHP dependencies (if you have any).
- Use Gulp to concatenate/minify any SASS and JavaScript. ([Need help?](https://bitbucket.org/lexiconitkonsult/litk-code-guidelines/src/ff8a3fa2b74981fee5e235539753d0ab1843e72e/help/gulp.md))

## Scope

Your plugin should be fully responsive support the latest Chrome, Safari and Firefox as well as IE11/Edge+ and should work in the latest mobile versions of Chrome and Safari.

## Delivery

Please submit your plugin by e-mailing us a URL to your public (open source) [bitbucket.org](http://bitbucket.org) **forked** version of this repository ```litk-wp-test```, with your working code.

**To be clear:**

1. Fork this git-repo, ([Need help?](http://lmgtfy.com/?q=bitbucket+fork+repository))
2. Do some coding! Yey! ;-)
3. Send e-mail to your contact at Lexicon IT-konsult with URL to your forked version.

Repository should be ready to use after cloning into the Wordpress plugin folder.