var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var cssnano = require('gulp-cssnano');

gulp.task('gen-css', function(){
  return gulp.src('source/sass/style.scss')
  	.pipe(sass())
  	.pipe(cssnano())
    .pipe(gulp.dest('dist/css/'))
});

gulp.task('gen-js', function(){
  return gulp.src('source/js/script.js')
  	.pipe(uglify())
    .pipe(gulp.dest('dist/js/'))
});