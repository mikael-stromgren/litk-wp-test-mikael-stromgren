<?php
/*
Plugin Name: LIKT Wp Test 2
Description: WordPress Test 2
Version:     1.0
Author:      Mikael Strömgren
*/

if (!defined('WPINC')) {
    die;
}

require_once(__DIR__ . '/vendor/autoload.php');

$timber = new \Timber\Timber();
Timber::$locations = __DIR__ . '/views/';

$subpages = new Subpages();