jQuery(document).ready(function($) {

    // Add control buttons
    $(".js-lims ul").before("<span class=\"lims-controller js-lims-sorter lims-sorter-do-desc\"></span>");
    $(".js-lims ul").before("<span class=\"lims-controller js-lims-toggler lims-toggler-do-close\"></span>");

    // Toggle visibility
    $(".js-lims-toggler" ).click(function() {
        $(this).parent().children("ul").toggle();
        $(this).toggleClass("lims-toggler-do-open", "lims-toggler-do-close"); 
    });

    // Sort alphabetically
    $(".js-lims-sorter" ).click(function() {
        var ul = $(this).parent().children("ul");
        ul.find(">li").sort($(this).hasClass("lims-sorter-do-asc") ? sort_asc : sort_desc).appendTo(ul);
        $(this).toggleClass("lims-sorter-do-asc", "lims-sorter-do-desc");
    });

    // Ascending sort
    function sort_asc(a, b){
        return ($(b).text().toUpperCase()) < ($(a).text().toUpperCase()) ? 1 : -1;    
    }

    // Descending sort
    function sort_desc(a, b){
        return ($(b).text().toUpperCase()) > ($(a).text().toUpperCase()) ? 1 : -1;    
    }

});