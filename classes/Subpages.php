<?php

/**
 * Class Subpages
 */
class Subpages {
    /**
     * Subpages constructor.
     */
    public function __construct() {
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_shortcode('litk-wp-test-mikael-stromgren', array($this, 'shortcode'));
        add_action( 'widgets_init', array($this, 'widget'));
    }

    /**
     * @return mixed
     */
    public function shortcode() {
        $data = array('pages' => $this->pages());
        return Timber::fetch('front/shortcodes/litk-wp-test-mikael-stromgren.twig', $data);
    }

    /**
     *
     */
    public function widget() {
        register_widget('SubpagesWidget');
    }

    /**
     *
     */
    public function scripts() {
        wp_register_style( 'lims-css', plugins_url('/../dist/css/style.css', __FILE__), array(), '1', 'all');   
        wp_register_script('lims-js', plugins_url('/../dist/js/script.js', __FILE__), array('jquery'));
        wp_enqueue_style('lims-css');
        wp_enqueue_script('lims-js');
    }

    /**
     * @return array
     */
    public function pages() {
        $args = array('child_of' => get_the_ID(), 'hierarchical' => 1, 'parent' => -1, 'sort_column' => 'post_title', 'sort_order' => 'asc');
        $pages = get_pages($args);
        $editedPages = array();
        foreach ($pages as $page) {
            $postTitle = (strlen($page->post_title) > 20) ? substr($page->post_title, 0, 20) . '...' : $page->post_title;
            $thumbnail = get_the_post_thumbnail_url($page->ID);
            $editedPages[] = array('ID' => $page->ID, 'post_parent' => $page->post_parent, 'post_title' => $postTitle, 'post_full_title' => $page->post_title, 
                'guid' => $page->guid, 'thumbnail' => $thumbnail);
        }
        return $editedPages; 
    }
}