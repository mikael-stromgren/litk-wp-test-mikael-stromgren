<?php

/**
 * Class SubpagesWidget
 */
class SubpagesWidget extends WP_Widget {

    /**
     * SubpagesWidget constructor.
     */
    public function __construct() {
        parent::__construct(
            'lims_subpage_widget',
            __('Display/sort subpages', 'text_domain'),
            array(
                'customize_selective_refresh' => true,
            )
        );
    }

    /**
     *
     */
    public function form() {
        Timber::render('subpageswidget/form.twig');
     }

    /**
     * @param $args
     * @param $instance
     */
    public function widget($args, $instance) {
        $pages = Subpages::pages();
        $data = array('args' => $args, 'pages' => $pages);
        Timber::render('subpageswidget/widget.twig', $data);
    }
}